import logging
import sys


logger = logging.getLogger("idq")


def add_workflow_options(parser, workflow, lookback):
    parser.add_argument(
        "-w",
        "--workflow",
        default=workflow,
        help="workflow for launching batch jobs",
    )
    parser.add_argument(
        "-i",
        "--initial-lookback",
        default=lookback,
        type=float,
        help=(
            "if causal batch is specified, that look back this much before "
            "t_start to use as a seed to evaluate starting at t_start."
        ),
    )
    parser.add_argument(
        "--skip-timeseries", action="store_true", help="do not generate timeseries"
    )
    parser.add_argument(
        "--skip-report", action="store_true", help="do not generate report"
    )


def add_bin_options(parser, num_bins, num_segs_per_bin):
    parser.add_argument(
        "-c", "--causal", action="store_true", help="use causal round-robin binning"
    )
    parser.add_argument(
        "-n",
        "--num-bins",
        default=num_bins,
        type=int,
        help=(
            "the number of round-robin bins to generate."
            "Divisions are made according to walltime"
        ),
    )
    parser.add_argument(
        "-N",
        "--num-segs-per-bin",
        default=num_segs_per_bin,
        type=int,
        help=(
            "the number of segments per bin within the round-robin procedure. "
            "If this is greater than 1, segments will be organized in a checkerboard "
            "pattern in order to sample from the entire range in both training "
            "and evaluation. Note, this is only used if --causal is NOT supplied."
        ),
    )


def add_logging_options(parser):
    """Add logging client options to an argument parser."""
    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        "-q",
        "--quiet",
        action="store_true",
        help="If set, only display warnings and errors.",
    )
    group.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="If set, display additional logging messages.",
    )


def add_batch_options(parser):
    parser.add_argument("config", metavar="CONFIG")
    parser.add_argument("start", metavar="START", type=int)
    parser.add_argument("end", metavar="END", type=int)


def add_stream_options(parser):
    parser.add_argument("config", metavar="CONFIG")
    parser.add_argument("start", metavar="START", type=int, nargs="?")
    parser.add_argument("end", metavar="END", type=int, nargs="?")


def format_args(args, positional=False, logging=False):
    kwargs = vars(args)
    if not positional:
        filter_positional_kwargs(kwargs)
    if not logging:
        filter_logging_kwargs(kwargs)
    return kwargs


def filter_logging_kwargs(kwargs):
    kwargs.pop("verbose")
    kwargs.pop("quiet")
    return kwargs


def filter_positional_kwargs(kwargs):
    kwargs.pop("config")
    kwargs.pop("start")
    kwargs.pop("end")
    return kwargs


def validate_times(args):
    if not args.end > args.start:
        raise ValueError(
            "end time ({args.end:%d}) must be greater than start time ({args.start:d})"
        )


def get_log_level(args):
    """Determine the log level from logging options."""
    if args.quiet:
        return logging.WARNING
    elif args.verbose:
        return logging.DEBUG
    else:
        return logging.INFO


def set_up_logger(args):
    """Set up common logging settings for CLI usage."""
    log_level = get_log_level(args)
    logger.setLevel(log_level)
    handler = logging.StreamHandler(sys.stderr)
    handler.setLevel(log_level)
    formatter = logging.Formatter("%(asctime)s | idq : %(levelname)s : %(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)
