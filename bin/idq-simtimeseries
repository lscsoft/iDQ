#!/usr/bin/env python

import argparse
import logging
import os

from idq import configparser
from idq import cli
from idq import factories
from idq import names
from idq import series
from idq import utils


DEFAULT_MODE = "zeros"
ALLOWED_MODES = ["zeros", "ones", "rand"]


logger = logging.getLogger("idq")

# command line options
parser = argparse.ArgumentParser()
cli.add_batch_options(parser)

parser.add_argument(
    "-v",
    "--verbose",
    action="store_true",
    help="print to stdout in addition to writing to automatically generated log",
)

parser.add_argument(
    "-m",
    "--mode",
    default="zerod",
    type=str,
    help="the mode in which we simulate timeseries. Can be one of: %s. \
DEFAULT=%s"
    % (", ".join(ALLOWED_MODES), DEFAULT_MODE),
)

# parse and validate
args = parser.parse_args()

cli.validate_times(args)
assert args.mode in ALLOWED_MODES, "--mode=%s not understood, must be one of %s\n" % (
    args.mode,
    ", ".join(ALLOWED_MODES),
)

# run
cli.set_up_logger(args)

# read in config
config = configparser.path2config(args.config)

# extract common parameters
tag = config.tag

rootdir = os.path.abspath(config.rootdir)
timeseriesdir = names.tag2timeseriesdir(tag, rootdir=rootdir)
logdir = names.tag2logdir(tag, rootdir=rootdir)

for directory in [rootdir, timeseriesdir, logdir]:
    if not os.path.exists(directory):
        os.makedirs(directory)

# extract information necessary for channel names in timeseries files
instrument = config.instrument

target_bounds = configparser.config2bounds(config.samples["target_bounds"])
assert (
    "frequency" in target_bounds
), "must specify a frequency range within target_bounds"
freq_min, freq_max = target_bounds["frequency"]
assert isinstance(freq_min, int) and isinstance(
    freq_max, int
), "frequency bounds must be integers!"

logger.info("using config : %s" % args.config)

# figure out which classifiers we're faking
nicknames = config.classifiers
logger.info("simulating data for " + ", ".join(nicknames))

# set up how we record results
reporter_factory = factories.ReporterFactory()
timeseriesreporter = reporter_factory(
    timeseriesdir, args.start, args.end, **config.timeseries["reporting"]
)

# set up our strides, etc
srate = config.timeseries["srate"]
dt = 1.0 / srate

stride = config.timeseries["stream"].get("stride", utils.DEFAULT_STRIDE)
delay = config.timeseries["stream"].get("delay", utils.DEFAULT_DELAY)

manager = utils.CadenceManager(
    args.start, max_timestamp=args.end, stride=stride, delay=delay, logger=logger
)

### actually generate the dummy timeseries object
if args.mode == "zeros":
    generate = series.SeriesInfo.generate_zeros
elif args.mode == "ones":
    generate = series.SeriesInfo.generate_ones
elif args.mode == "rand":
    generate = series.SeriesInfo.generate_rand
else:
    raise ValueError("--mode=%s not understood" % args.mode)

logger.info("generating simulated timeseries for [%.3f, %.3f)" % (args.start, args.end))
while manager.timestamp < args.end:
    for seg in manager.poll():
        timeseriesreporter.start, timeseriesreporter.end = seg
        for nickname in nicknames:
            start, end = seg
            info = series.ChannelInfo(instrument, nickname, freq_min, freq_max)
            timeseries = generate(info, start, end, dt)
            path = timeseriesreporter.report(
                f"{instrument}-{nickname}", timeseries, preferred=True
            )
            logger.info("timeseries written to %s" % path)
