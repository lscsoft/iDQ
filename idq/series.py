from __future__ import annotations

from dataclasses import dataclass
from typing import Optional

from ligo.segments import segment, segmentlist
import numpy as np

from . import names


@dataclass
class ChannelInfo:
    detector: str
    f_low: int
    f_high: int
    nickname: str
    description: Optional[str] = None

    @property
    def pglitch(self):
        return self._metric_to_channel_name(names.PGLITCH)

    @property
    def loglike(self):
        return self._metric_to_channel_name(names.LOGLIKE)

    @property
    def eff(self):
        return self._metric_to_channel_name(names.EFF)

    @property
    def fap(self):
        return self._metric_to_channel_name(names.FAP)

    @property
    def rank(self):
        return self._metric_to_channel_name(names.RANK)

    @property
    def ok(self):
        return self._metric_to_channel_name(names.OK)

    @property
    def file_nickname(self):
        if self.description:
            return f"{self.detector[0]}-{self.description}"
        else:
            return f"{self.detector[0]}-{self.nickname}"

    @classmethod
    def from_channel(cls, channel):
        detector, name = channel.split(":")
        identifier = name.split("-")[1]
        _, nickname, f_low, f_high = identifier.split("_")
        return cls(detector, nickname.lower(), f_low, f_high)

    def channel_to_metric_name(self, channel):
        identifier = channel.split("-")[1]
        return identifier.split("_")[0].lower()

    def _metric_to_channel_name(self, metric):
        f_range = f"{self.f_low}_{self.f_high}"
        return f"{self.detector}:IDQ-{metric}_{self.nickname.upper()}_{f_range}"


@dataclass
class Series:
    channel: str
    series: np.ndarray


@dataclass
class SeriesInfo:
    t0: int
    dt: float
    info: ChannelInfo
    pglitch: Series
    loglike: Series
    eff: Series
    fap: Series
    rank: Series
    ok: Series
    model_id: Optional[str] = None
    calibration_id: Optional[str] = None

    @property
    def start(self):
        return self.t0

    @property
    def end(self):
        return self.t0 + self.dt * len(self)

    @property
    def times(self):
        return np.arange(self.start, self.end, self.dt)

    @property
    def channels(self):
        return self.keys()

    def keys(self):
        return (
            self.pglitch.channel,
            self.loglike.channel,
            self.eff.channel,
            self.fap.channel,
            self.rank.channel,
            self.ok.channel,
        )

    def values(self):
        return (
            self.pglitch.series,
            self.loglike.series,
            self.eff.series,
            self.fap.series,
            self.rank.series,
            self.ok.series,
        )

    def items(self):
        return zip(self.keys(), self.values())

    def __len__(self):
        return self.pglitch.series.size

    def crop(self, start=None, end=None):
        mask = self.times >= start
        mask *= self.times < end

        self.t0 = self.times[mask][0]
        self.pglitch.series = self.pglitch.series[mask]
        self.loglike.series = self.loglike.series[mask]
        self.eff.series = self.eff.series[mask]
        self.fap.series = self.fap.series[mask]
        self.rank.series = self.rank.series[mask]
        self.ok.series = self.ok.series[mask]

        return self

    @classmethod
    def from_ranks(cls, info, t0, dt, ranks, model, calibration_map, set_ok=None):
        if set_ok is None:
            set_ok = calibration_map.is_healthy()
        return cls(
            t0,
            dt,
            info,
            pglitch=Series(info.pglitch, calibration_map.pglitch(ranks)),
            loglike=Series(info.loglike, calibration_map.loglike(ranks)),
            eff=Series(info.eff, calibration_map.eff(ranks)),
            fap=Series(info.fap, calibration_map.fap(ranks)),
            rank=Series(info.rank, ranks),
            ok=Series(
                info.ok,
                np.ones(len(ranks)) if set_ok else np.zeros(len(ranks)),
            ),
            model_id=model.hash,
            calibration_id=calibration_map.hash,
        )

    @classmethod
    def generate_ones(cls, info, start, end, dt):
        n = (end - start) // dt
        return cls(
            info,
            start,
            dt,
            model_id=None,
            calibration_id=None,
            pglitch=Series(info.pglitch, np.ones(n, dtype=float)),
            loglike=Series(info.loglike, np.inf * np.ones(n, dtype=float)),
            eff=Series(info.eff, np.ones(n, dtype=float)),
            fap=Series(info.fap, np.zeros(n, dtype=float)),
            rank=Series(info.rank, np.ones(n, dtype=float)),
            ok=Series(info.ok, np.ones(n, dtype=float)),
        )

    @classmethod
    def generate_zeros(cls, info, start, end, dt):
        n = (end - start) // dt
        return cls(
            info,
            start,
            dt,
            model_id=None,
            calibration_id=None,
            pglitch=Series(info.pglitch, np.zeros(n, dtype=float)),
            loglike=Series(info.loglike, -np.inf * np.ones(n, dtype=float)),
            eff=Series(info.eff, np.zeros(n, dtype=float)),
            fap=Series(info.fap, np.ones(n, dtype=float)),
            rank=Series(info.rank, np.zeros(n, dtype=float)),
            ok=Series(info.ok, np.ones(n, dtype=float)),
        )

    @classmethod
    def generate_rand(cls, info, start, end, dt):
        n = (end - start) // dt
        pglitch = np.random.rand(n)
        return cls(
            info,
            start,
            dt,
            model_id=None,
            calibration_id=None,
            pglitch=Series(info.loglike, 1 - pglitch),
            loglike=Series(info.pglitch, np.log(pglitch) - np.log(1 - pglitch)),
            eff=Series(info.eff, pglitch),
            fap=Series(info.fap, 1 - pglitch),
            rank=Series(info.rank, pglitch),
            ok=Series(info.ok, np.ones(n, dtype=float)),
        )


def combine_series(all_series):
    # check if combining is needed
    if len(all_series) < 2:
        return all_series

    out = []
    current = all_series[0]

    for series in all_series[1:]:
        if (
            (series.start == current.end)
            and (series.dt == current.dt)
            and (series.model_id == current.model_id)
            and (series.calibration_id == current.calibration_id)
        ):
            # concatenate the current series
            current.pglitch.series = np.concatenate(
                [current.pglitch.series, series.pglitch.series]
            )
            current.loglike.series = np.concatenate(
                [current.loglike.series, series.loglike.series]
            )
            current.eff.series = np.concatenate([current.eff.series, series.eff.series])
            current.fap.series = np.concatenate([current.fap.series, series.fap.series])
            current.rank.series = np.concatenate(
                [current.rank.series, series.rank.series]
            )
            current.ok.series = np.concatenate([current.ok.series, series.ok.series])
        else:
            # process the next series
            out.append(current)
            current = series

    out.append(current)
    return out


def extract_segments_from_series(all_series):
    segs = segmentlist([segment(series.start, series.end) for series in all_series])
    return segs.coalesce()
