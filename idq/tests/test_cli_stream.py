def test_cli_stream(script_runner):
    ret = script_runner.run("idq-stream", "--help")
    assert ret.success


def test_cli_stream_train(script_runner):
    ret = script_runner.run("idq-streaming_train", "--help")
    assert ret.success


def test_cli_stream_evaluate(script_runner):
    ret = script_runner.run("idq-streaming_evaluate", "--help")
    assert ret.success


def test_cli_stream_calibrate(script_runner):
    ret = script_runner.run("idq-streaming_calibrate", "--help")
    assert ret.success


def test_cli_stream_timeseries(script_runner):
    ret = script_runner.run("idq-streaming_timeseries", "--help")
    assert ret.success


def test_cli_stream_report(script_runner):
    ret = script_runner.run("idq-streaming_report", "--help")
    assert ret.success
