.. _references:

References
####################################################################################################

A list of references to original publications, algorithmic descriptions for supervised classifiers, etc.
Should serve as a general bibliography for the interested reader.

* Biswas et al. *Application of machine learning algorithms to the study of noise artifacts in gravitational-wave data.* PRD **88**, 062003 (2013).
* Essick, Blackburn, and Katsavounidis. *Optimizing vetoes for gravitational-wave transient searches.* CQG **30**, 15 (2013). 
* Stephens, Flores Huerta, and Ruiz Linares. *When is the Naive Bayes approximation not so naive?* Machine Learning (2017). 
* Zhang. *The Optimality of Naive Bayes.* In FLAIRS2004 conference.
* Kupervasser. *The mysterious optimality of naive bayes: estimation of the probability in the system of "classifiers".* Pattern Recognition and Image Analysis **24**, 1, 1-10 (2014).
