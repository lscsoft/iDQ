def test_cli_check_config(script_runner):
    ret = script_runner.run("idq-check_config", "--help")
    assert ret.success


def test_cli_simtimeseries(script_runner):
    ret = script_runner.run("idq-simtimeseries", "--help")
    assert ret.success


def test_cli_target_times(script_runner):
    ret = script_runner.run("idq-target-times", "--help")
    assert ret.success
