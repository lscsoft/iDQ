def path2channels(path):
    """
    extracts a list of channels from a path
    expects each row to be a single channel name
    returns a list
    """
    with open(path, "r") as file_obj:
        channels = [_.strip() for _ in file_obj.readlines()]

    return channels
