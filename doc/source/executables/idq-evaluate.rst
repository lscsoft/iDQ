.. _idq-evaluate:

idq-evaluate
####################################################################################################

describe script, which modules it relies upon, etc.

.. graphviz::

   digraph idq_evaluate {
      labeljust = "r";
      label=""
      rankdir=LR;
      graph [fontname="Roman", fontsize=24];
      edge [ fontname="Roman", fontsize=10 ];
      node [fontname="Roman", shape=box, fontsize=11];
      style=rounded;
      labeljust = "r";
      fontsize = 14;


      DataSrc [label="auxiliary features"];
      Model [label="model"]
      Evaluate [label="idq-evaluate"];
      Quiver [label="quiver"];

      DataSrc -> Evaluate;
      Model -> Evaluate;
      Evaluate -> Quiver;

   }

.. program-output:: idq-evaluate --help
   :nostderr:
