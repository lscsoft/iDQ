.. _api:

iDQ API
####################################################################################################

We separate data discovery (:doc:`io`), feature extraction (:doc:`features`), and classification (:doc:`classifiers/classifiers`) into separate modules for clarity.
Our object-oriented architecture means that the :class:`idq.classifiers.SupervisedClassifier` object has responsibility for quite a few tasks, including

* evaluation of ranks or generation of timeseries, 
* supervised calibration of ranks or rank timeseries, and
* supervised training.

We also include a few other modules that contain helper functions (:doc:`utils`), naming convetions (:doc:`names`), and common code for measuring histograms and estimating probability density functions (:doc:`calibration`).

.. toctree::
    :maxdepth: 1

    batch
    calibration
    classifiers/classifiers
    condor
    configparser
    factories
    features
    io
    logs
    names
    plots
    stream
    utils
