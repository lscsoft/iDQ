.. _features:

idq.features
####################################################################################################

`features.py` supports the class representation of :class:`idq.features.FeatureVector` and :class:`idq.features.Quiver`, which are thin wrappers around `numpy structured arrays`.

.. _features-docstrings:

.. automodule:: idq.features
    :members:
