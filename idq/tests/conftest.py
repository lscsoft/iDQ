import json
import os
import tempfile
import time
import pytest

import h5py
import numpy as np

from ligo import segments as ligo_segments

from idq import configparser
from idq import factories
from idq import features
from idq import stream
from idq import utils
from idq.io import triggers
from idq.io.triggers import snax


def create_snax_data(rootdir, **kwargs):
    """
    creates SNAX-based data for use in testing
    """
    # grab keyword arguments, filling in defaults as necessary
    basename = kwargs.pop("basename", "H-SNAX_TRIGGERS")
    channels = kwargs.pop("channels", [])
    columns = kwargs.pop("columns", [])
    start_time = kwargs.pop("start_time", 0)
    end_time = kwargs.pop("end_time", 100)
    dset_span = kwargs.pop("dataset_span", 100)
    dset_per_file = kwargs.pop("datasets_per_file", 1)
    sample_rate = kwargs.pop("sample_rate", 1)

    # create datasets
    datasets = []
    names = utils.segs2datasets(
        ligo_segments.segmentlist([ligo_segments.segment(start_time, end_time)]),
        stride=dset_span,
    )
    for dataset_name in names:
        # get dataset times
        dataset_name = dataset_name[0]
        start_time, end_time = utils.start_dur2start_end(dataset_name)

        # define structure of dataset
        triggers = {
            channel: np.empty(
                (dset_span,), dtype=[(column, "f8") for column in columns]
            )
            for channel in channels
        }

        # fill in dataset with sensible values
        for channel in channels:
            if "trigger_time" in columns:
                times = np.linspace(
                    start_time, end_time, num=dset_span, endpoint=False
                ) + np.random.rand(dset_span)
                triggers[channel]["trigger_time"] = times

            for column in columns:
                if column != "trigger_time":
                    triggers[channel][column] = 10 * np.random.rand(dset_span)

        # define path in appropriate way
        filename_start, filename_end = utils.time2window(
            start_time, dset_span * dset_per_file
        )
        filename = to_snax_trigger_path(
            rootdir, basename, "0001", filename_start, filename_end - filename_start
        )

        # remove file if it already exists
        if os.path.isfile(filename):
            os.remove(filename)

        datasets.append((filename, dataset_name, triggers))

    # save datasets to disk
    for filename, dataset_name, triggers in datasets:
        with h5py.File(filename, "a") as file_obj:
            if not file_obj.attrs:
                file_obj.attrs.create("cadence", dset_span)
                file_obj.attrs.create("sample_rate", sample_rate)

            for channel in channels:
                group = channel

                if group not in file_obj:
                    file_obj.create_group(group)

                file_obj[group].create_dataset(dataset_name, data=triggers[channel])


def to_snax_trigger_path(rootdir, basename, job_id, start, dur):
    """
    returns a path that matches the convention expected for SNAX-based triggers,
    creating any directories if needed
    """
    gps_mod1e5 = start / 100000
    filename = "%s-%d-%d.h5" % (basename, start, dur)
    basepath = os.path.join(
        rootdir,
        basename,
        basename + "-" + str(gps_mod1e5),
        "-".join([basename, job_id]),
    )

    # create directories
    os.makedirs(basepath, exist_ok=True)

    return os.path.join(basepath, filename)


@pytest.fixture(scope="session")
def snax_data_conf():
    start_time = 1000000000
    dset_span = 100

    return {
        "rootdir": os.path.join(tempfile.gettempdir(), "tmp-features"),
        "basename": "H-GSTLAL_IDQ_TRIGGERS",
        "dataset_span": dset_span,
        "datasets_per_file": 2,
        "start_time": start_time,
        "end_time": start_time + 6 * dset_span,
        "channels": ["channel1", "channel2", "channel3"],
        "columns": ["time", "snr", "frequency"],
    }


@pytest.fixture(scope="session")
def kafka_dataloader_conf():
    start_time = 1000000000
    end_time = 1000000005
    channels = ["channel1", "channel2", "channel3"]
    columns = ["time", "snr", "frequency"]

    return {
        "start_time": start_time,
        "end_time": end_time,
        "channels": channels,
        "columns": columns,
        "consumer": MockKafkaSource(channels, columns, start_time=start_time),
        "group": "group_1",
        "url": "localhost:9092",
        "topic": "test_topic",
        "sample_rate": 1,
        "poll_timeout": 0.1,
        "stride": 1,
        "retry_cadence": 0.1,
    }


@pytest.fixture(scope="session")
def snax_dataloader_conf():
    start_time = 1000000000
    dset_span = 100
    end_time = start_time + 6 * dset_span
    names = utils.segs2datasets(
        ligo_segments.segmentlist([ligo_segments.segment(start_time, end_time)]),
        stride=dset_span,
    )
    dset_spans = [name[1] for name in names]

    return {
        "dataset_span": dset_span,
        "datasets_per_file": 2,
        "start_time": start_time,
        "end_time": end_time,
        "sample_rate": 1,
        "rootdir": os.path.join(tempfile.gettempdir(), "tmp-features"),
        "basename": "H-GSTLAL_IDQ_TRIGGERS",
        "channels": ["channel1", "channel2", "channel3"],
        "columns": ["time", "snr", "frequency"],
        "segs": ligo_segments.segmentlist(
            [ligo_segments.segment(start_time + 30, end_time - 90)]
        ),
        "cadence": dset_span,
        "stride": dset_span,
        "datasets": dset_spans,
    }


@pytest.fixture(scope="session")
def dataloader_conf():
    start_time = 1000000000

    return {
        "rootdir": os.path.join(tempfile.gettempdir(), "tmp-dataset1"),
        "start_time": start_time,
        "end_time": start_time + 1000,
        "channels": ["channel1", "channel2", "channel3"],
        "columns": ["time", "snr", "frequency"],
        "segs": ligo_segments.segmentlist(
            [ligo_segments.segment(start_time + 30, start_time + 90)]
        ),
    }


@pytest.fixture(scope="session")
def snax_dataset_conf(snax_dataloader_conf):
    channels = ["channel1", "channel2", "channel3"]

    return {
        "channels": channels,
        "target_channel": channels[0],
        "safe_channels": channels[1:],
        "target_bounds": configparser.config2bounds({"snr": [7, "inf"]}),
        "dirty_bounds": configparser.config2bounds({"snr": [6, "inf"]}),
        "dirty_window": 0.2,
        "time": "time",
        "significance": "snr",
        "rate": 0.1,
        "dt": 2,
        "window": 0.2,
        "default": -10,
    }


@pytest.fixture(scope="class")
def test_segments(request):
    # segments
    seg_single = ligo_segments.segment(1000000, 2000000)
    seg_part1 = ligo_segments.segment(1000000, 1428321.3)
    seg_part2 = ligo_segments.segment(1573027.7, 1893702)
    seg_part3 = ligo_segments.segment(1938002, 2000000)

    # empty seglist
    seglist_empty = ligo_segments.segmentlist([])

    # continuous seglist
    seglist_single = ligo_segments.segmentlist([seg_single])

    # scattered seglist
    seglist_multiple = ligo_segments.segmentlist([seg_part1, seg_part2, seg_part3])

    # inject class variables
    request.cls.seg_single = seg_single
    request.cls.seg_part1 = seg_part1
    request.cls.seg_part2 = seg_part2
    request.cls.seg_part3 = seg_part3

    request.cls.seglist_empty = seglist_empty
    request.cls.seglist_single = seglist_single
    request.cls.seglist_multiple = seglist_multiple


@pytest.fixture(scope="class")
def kafka_dataloader(request, kafka_dataloader_conf):
    start_time = kafka_dataloader_conf["start_time"]
    end_time = kafka_dataloader_conf["end_time"]

    dataloader = snax.SNAXKafkaDataLoader(start_time, end_time, **kafka_dataloader_conf)

    # inject class variable
    request.cls.dataloader = dataloader


@pytest.fixture(scope="class")
def snax_dataloader(request, snax_data_conf, snax_dataloader_conf):
    conf = {key: value for key, value in snax_data_conf.items() if key != "rootdir"}
    create_snax_data(snax_data_conf["rootdir"], **conf)

    start_time = snax_dataloader_conf["start_time"]
    end_time = snax_dataloader_conf["end_time"]

    dataloader = snax.SNAXDataLoader(start_time, end_time, **snax_dataloader_conf)

    # inject class variable
    request.cls.dataloader = dataloader


@pytest.fixture(scope="class")
def dataloader(request):
    start_time = 1000000000
    end_time = start_time + 1000
    dataloader = triggers.DataLoader(start_time, end_time)

    # inject class variable
    request.cls.dataloader = dataloader


@pytest.fixture(scope="class")
def stream_processor(request, kafka_dataloader_conf):
    start_time = kafka_dataloader_conf["start_time"]
    duration = (
        kafka_dataloader_conf["end_time"] - kafka_dataloader_conf["start_time"]
    ) * 10
    end_time = kafka_dataloader_conf["start_time"] + duration
    factory = KafkaDataLoaderFactory()

    stream_processor = stream.StreamProcessor(
        start_time, end_time, factory, buffer_kwargs=kafka_dataloader_conf
    )

    # inject class variable
    request.cls.stream_processor = stream_processor


@pytest.fixture(scope="class")
def snax_dataset(request, snax_data_conf, snax_dataloader_conf, snax_dataset_conf):
    conf = {key: value for key, value in snax_data_conf.items() if key != "rootdir"}
    create_snax_data(snax_data_conf["rootdir"], **conf)

    start_time = snax_dataloader_conf["start_time"]
    end_time = snax_dataloader_conf["end_time"]

    dataloader = snax.SNAXDataLoader(start_time, end_time, **snax_dataloader_conf)

    target_channel = snax_dataset_conf["target_channel"]
    target_bounds = snax_dataset_conf["target_bounds"]
    dirty_bounds = snax_dataset_conf["dirty_bounds"]
    dirty_window = snax_dataset_conf["dirty_window"]
    time = snax_dataset_conf["time"]
    rate = snax_dataset_conf["rate"]

    target_times = dataloader.target_times(time, target_channel, target_bounds)
    random_times, _ = dataloader.random_times(
        time, target_channel, dirty_bounds, dirty_window, rate
    )

    labeled_dataset = factories.DatasetFactory(dataloader).labeled(
        target_times, random_times
    )
    unlabeled_dataset = factories.DatasetFactory(dataloader).unlabeled(
        dt=snax_dataset_conf["dt"]
    )

    selector = features.Selector(
        channels=dataloader.channels,
        time=time,
        downselector=features.DownselectLoudest(**snax_dataset_conf),
        transformer=features.DeltaTimeTransformer(**snax_dataset_conf),
    )
    labeled_dataset.configure(selector)
    unlabeled_dataset.configure(selector)

    # inject class variables
    request.cls.dataloader = dataloader
    request.cls.labeled_dataset = labeled_dataset
    request.cls.unlabeled_dataset = unlabeled_dataset
    request.cls.target_times = target_times
    request.cls.random_times = random_times
    request.cls.select = selector.downselector


class KafkaDataLoaderFactory(object):
    """
    Fakes a factory classifierData object that produces KafkaDataLoader objects
    """

    def __call__(self, start, end, **kwargs):
        return snax.SNAXKafkaDataLoader(start, end, **kwargs)


class MockKafkaSource(object):
    """
    Fakes a kafka source, and produces a random number of JSON packets
    when poll() is called.
    """

    def __init__(
        self, channels, columns, start_time=0, expected_packets=1, expected_time=0.01
    ):
        self.expected_packets = expected_packets
        self.expected_time = expected_time
        self.timestamp = start_time
        # hard-coded for now until higher sample rates are worked in
        self.sample_rate = 1
        self.channels = channels
        self.columns = columns

    def poll(self, timeout=None):
        for packet in [
            self._generate_packet()
            for _ in range(np.random.poisson(lam=self.expected_packets))
        ]:
            latency = np.random.exponential(scale=self.expected_time)
            if timeout and latency > timeout:
                time.sleep(timeout)
                return None
            else:
                time.sleep(np.random.exponential(scale=self.expected_time))
                return packet

    def offsets_for_times(self, kafka_partition):
        return kafka_partition

    def subscribe(self, topics):
        return

    def assign(self, topic_partitions):
        return

    def seek(self, kafka_partition):
        self.timestamp = (kafka_partition.offset / 1000.0) + np.random.poisson(lam=1)

    def _generate_packet(self):
        packet = {
            "timestamp": self.timestamp,
            "features": {
                channel: [self._generate_row(channel) for _ in range(self.sample_rate)]
                for channel in self.channels
            },
        }
        self.timestamp += 1
        return KafkaPacket(json.dumps(packet))

    def _generate_row(self, channel):
        row = {
            col: (
                self.timestamp + np.random.rand()
                if col == "trigger_time"
                else 10 * np.random.rand()
            )
            for col in self.columns
        }
        row["channel"] = channel
        return row


class KafkaPacket(object):
    """
    Fakes a Kafka packet.
    """

    def __init__(self, packet):
        self.packet = packet

    def error(self):
        return False

    def value(self):
        return self.packet
